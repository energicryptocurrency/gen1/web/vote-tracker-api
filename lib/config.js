'use strict';

const yaml = require('js-yaml');
const fs = require('fs');

try {
  const config = yaml.safeLoad(fs.readFileSync('./config.yml', 'utf8'));
  module.exports = config;
} catch (e) {
  console.log(e);
}
