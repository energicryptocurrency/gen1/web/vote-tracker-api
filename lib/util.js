const RpcClient = require('@energicryptocurrency/energid-rpc');
const EventEmitter = require('events');
const { energiRPC } = require('./config');

const rpc = new RpcClient(energiRPC);

const CALL = Symbol();

const cached = (fun) => {
    let last_value = null;
    let last_time = [0, 0];
    const ee = new EventEmitter();
    ee.setMaxListeners(0);
    
    return (cb) => {
        if (process.hrtime(last_time)[0] <= 0) {
            cb(undefined, last_value);
            return;
        }

        ee.once(CALL, cb);

        if (ee.listenerCount(CALL) == 1) {
            fun((err, info) => {
                if (!err) {
                    last_value = info;
                    last_time = process.hrtime();
                }

                ee.emit(CALL, err, info);
            });
        }
    };
};

// functions to be exported
module.exports = {
/*
  getDifficulty: function () {
    rpc.getDifficulty(function (err, difficulty) {
      if (err) {
        return console.error(err);
      }

      console.log('Difficulty: ' + JSON.stringify(difficulty));
      return;
    });
  },
*/
  //get masternode list
  getMasternodeList: cached((cb) => {
    return rpc.masternodelist('full', cb);
  }),

  //get governance info
  getGovernanceInfo: cached((cb) => {
    return rpc.getGovernanceInfo(cb);
  }),

  //get superblockcycle
  getSuperBlockCycle: cached((cb) => {
    var blockCycle = 20160; //mainnet cycle by default
    rpc.getGovernanceInfo((err, info) => {
      if (err || !info) {
          console.error(err)
          console.error('Couldn\'t get the superblockcycle. Will resort to the default value.' + err);
        } else if (info.result && info.result.superblockcycle) {
          blockCycle = info.result.superblockcycle;
        }
        return cb(blockCycle * 60, err); //TODO: Do we need to return a value(default) if error occures ?
    });
  }),

  // get current block height
  getBlockCount: cached((cb) => {
    return rpc.getBlockCount(cb);
  }),
  // get governance objects
  getGovernanceObjectList: cached((cb) => {
    return rpc.gobject('list', cb);
  }),
}
